document.addEventListener("backbutton", onBackKeyDown, false);
function onBackKeyDown() {
    navigator.notification.confirm(
    '确认退出？', // message
     exitBlogApp,            // callback to invoke with index of button pressed
    '退出',           // title
    ['确定','取消']     // buttonLabels
    );
}
function exitBlogApp(buttonIndex){
    if(buttonIndex===1){
        navigator.app.exitApp();
    }
}