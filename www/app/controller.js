eyasBlog.controller('PostListCtrl', ['$scope', '$http', 'siteConfig',
    function($scope, $http, siteConfig) {
        $scope.conf = siteConfig;
        if (localStorage.getItem('posts') == null) {
            $http.get(siteConfig.dataRootUrl).success(function(data) {
                $scope.posts = data;
                localStorage.setItem('posts', JSON.stringify(data));
            });
        } else {
            $scope.posts = JSON.parse(localStorage.getItem('posts'))

        }
    }
]);
eyasBlog.controller('PostDetailCtrl', ['$scope', '$http', '$stateParams', 'siteConfig',
    function($scope, $http, $stateParams, siteConfig) {
        if (localStorage.getItem('posts-' + $stateParams.pid) == null) {

            $http.get(siteConfig.dataRootUrl + "&qv[p]=" + $stateParams.pid).success(function(data) {
                $scope.post = data;
                localStorage.setItem('post-' + $stateParams.pid, JSON.stringify(data));
            });
        } else {
            $scope.post = JSON.parse(localStorage.getItem('posts'))
        }
        $scope.orderList = 'post_date';
    }
]);
eyasBlog.controller('PostCateCtrl', ['$scope', '$http', '$stateParams', 'siteConfig',
    function($scope, $http, $stateParams, siteConfig) {
        if (localStorage.getItem('posts-cate-' + $stateParams.cid) == null) {
            $http.get(siteConfig.dataRootUrl + "&qv[cat]=" + $stateParams.cid).success(function(data) {
                console.log(data);
                $scope.posts = data;
                localStorage.setItem('post-cate-' + $stateParams.cid, JSON.stringify(data));
            });
        } else {
            $scope.post = JSON.parse(localStorage.getItem('posts'))
        }
        $scope.orderList = 'post_date';
    }
]);
eyasBlog.controller('HeaderCtrl', ['$scope', 'siteConfig',
    function($scope, siteConfig) {
        $scope.golbal = siteConfig;
    }
]);
