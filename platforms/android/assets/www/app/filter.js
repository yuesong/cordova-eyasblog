eyasBlog.filter('saveHtml', function() {
    return function(str) {
        // strip script/html tags
        str = str.replace(/<script[^>]*>([\S\s]*?)<\/script>/gmi, '');
        str = str.replace(/<\/?[^>]*>/g, ''); //去除HTML tag
        str = str.replace(/[ | ]*\n/g, '\n'); //去除行尾空白
        str = str.replace(/\n[\s| | ]*\r/g, '\n'); //去除多余空行
        return str;
    };
});
eyasBlog.filter('abstract', function() {
    return function(str, length) {
        if (str.length > length) {
            return str.substr(0, length) + '...';
        }
        return str;
    }
});
